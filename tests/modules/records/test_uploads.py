# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
from io import BytesIO

import pytest

import kadi.lib.constants as const
from kadi.lib.exceptions import KadiChecksumMismatchError
from kadi.lib.exceptions import KadiFilesizeMismatchError
from kadi.lib.security import hash_value
from kadi.lib.storage.local import create_chunk_storage
from kadi.modules.records.models import FileState
from kadi.modules.records.models import Upload
from kadi.modules.records.models import UploadState
from kadi.modules.records.models import UploadType
from kadi.modules.records.uploads import delete_upload
from kadi.modules.records.uploads import merge_chunk_data
from kadi.modules.records.uploads import remove_upload
from kadi.modules.records.uploads import save_chunk_data
from kadi.modules.records.uploads import save_upload_data


def test_delete_upload(dummy_upload):
    """Test if deleting uploads works correctly."""
    delete_upload(dummy_upload)
    assert dummy_upload.state == UploadState.INACTIVE


def test_remove_upload(new_upload):
    """Test if removing uploads works correctly."""
    file_data = 10 * b"x"

    upload = new_upload(size=len(file_data))
    save_chunk_data(upload, 0, len(file_data), BytesIO(file_data))
    remove_upload(upload)

    assert not Upload.query.all()
    assert not os.listdir(upload.storage.root_directory)

    chunk_storage = create_chunk_storage()

    assert not os.listdir(chunk_storage.root_directory)


def test_save_chunk_data_success(dummy_upload):
    """Test if saving valid chunk data works correctly."""
    file_data = 10 * b"x"
    index = 0

    save_chunk_data(dummy_upload, index, len(file_data), BytesIO(file_data))

    chunk_storage = create_chunk_storage()
    filepath = chunk_storage.create_filepath(f"{dummy_upload.id}-{index}")

    assert chunk_storage.exists(filepath)


@pytest.mark.parametrize(
    "file_data,size,checksum,exception",
    [
        (b"xx", 1, None, KadiFilesizeMismatchError),
        (b"x", 1, "test", KadiChecksumMismatchError),
    ],
)
def test_save_chunk_data_error(file_data, size, checksum, exception, dummy_upload):
    """Test if saving invalid chunk data works correctly."""
    with pytest.raises(exception):
        save_chunk_data(dummy_upload, 0, size, BytesIO(file_data), checksum=checksum)


def test_merge_chunk_data_success(dummy_record, new_upload):
    """Test if merging valid chunk data works correctly."""
    file_data = 10 * b"x"
    prev_timestamp = dummy_record.last_modified

    upload = new_upload(
        size=len(file_data),
        description="test",
        checksum=hash_value(file_data, alg="md5"),
        mimetype="test/test",
    )
    save_chunk_data(upload, 0, len(file_data), BytesIO(file_data))
    file = merge_chunk_data(upload)

    assert dummy_record.last_modified > prev_timestamp
    assert upload.state == UploadState.INACTIVE
    assert file.state == FileState.ACTIVE
    assert file.description == "test"
    assert file.mimetype == "test/test"
    assert file.magic_mimetype == const.MIMETYPE_TEXT
    assert file.revisions.count() == 1
    assert file.storage.exists(file.storage.create_filepath(str(file.id)))

    # Replace the previous file.
    file_data = b'{"foo": [], "bar": "baz"}'
    prev_timestamp = dummy_record.last_modified
    prev_file_id = file.id

    upload = new_upload(
        file=file,
        size=len(file_data),
        description="test2",
        checksum=hash_value(file_data, alg="md5"),
    )
    save_chunk_data(upload, 0, len(file_data), BytesIO(file_data))
    file = merge_chunk_data(upload)

    assert dummy_record.last_modified > prev_timestamp
    assert upload.state == UploadState.INACTIVE
    assert file.state == FileState.ACTIVE
    assert file.description == "test2"
    assert file.mimetype == const.MIMETYPE_TEXT
    assert file.magic_mimetype == const.MIMETYPE_JSON
    assert file.revisions.count() == 2
    assert file.storage.exists(file.storage.create_filepath(str(file.id)))
    assert file.id == prev_file_id


@pytest.mark.parametrize(
    "file_data,upload_size,checksum,exception",
    [
        (b"xx", 1, None, KadiFilesizeMismatchError),
        (b"x", 1, "test", KadiChecksumMismatchError),
    ],
)
def test_merge_chunk_data_error(
    file_data, upload_size, checksum, exception, db, dummy_file, new_upload
):
    """Test if merging invalid chunk data works correctly."""
    upload = new_upload(size=upload_size, checksum=checksum, file=dummy_file)
    save_chunk_data(upload, 0, len(file_data), BytesIO(file_data))

    # Start a new savepoint, so only the latest changes are rolled back.
    db.session.begin_nested()

    with pytest.raises(exception):
        merge_chunk_data(upload)

    assert upload.state == UploadState.INACTIVE
    assert dummy_file.state == FileState.ACTIVE
    assert dummy_file.revisions.count() == 1
    assert dummy_file.storage.exists(
        dummy_file.storage.create_filepath(str(dummy_file.id))
    )


def test_save_upload_data_success(dummy_record, new_upload):
    """Test if saving valid upload data works correctly."""
    file_data = 10 * b"x"
    prev_timestamp = dummy_record.last_modified

    upload = new_upload(
        size=len(file_data),
        description="test",
        checksum=hash_value(file_data, alg="md5"),
        mimetype="test/test",
        upload_type=UploadType.DIRECT,
    )
    file = save_upload_data(upload, BytesIO(file_data))

    assert dummy_record.last_modified > prev_timestamp
    assert upload.state == UploadState.INACTIVE
    assert file.state == FileState.ACTIVE
    assert file.description == "test"
    assert file.mimetype == "test/test"
    assert file.magic_mimetype == const.MIMETYPE_TEXT
    assert file.revisions.count() == 1
    assert file.storage.exists(file.storage.create_filepath(str(file.id)))

    # Replace the previous file.
    file_data = b'{"foo": [], "bar": "baz"}'
    prev_timestamp = dummy_record.last_modified
    prev_file_id = file.id

    upload = new_upload(
        file=file,
        size=len(file_data),
        description="test2",
        checksum=hash_value(file_data, alg="md5"),
    )
    file = save_upload_data(upload, BytesIO(file_data))

    assert dummy_record.last_modified > prev_timestamp
    assert upload.state == UploadState.INACTIVE
    assert file.state == FileState.ACTIVE
    assert file.description == "test2"
    assert file.mimetype == const.MIMETYPE_TEXT
    assert file.magic_mimetype == const.MIMETYPE_JSON
    assert file.revisions.count() == 2
    assert file.storage.exists(file.storage.create_filepath(str(file.id)))
    assert file.id == prev_file_id


@pytest.mark.parametrize(
    "file_data,upload_size,checksum,exception",
    [
        (b"xx", 1, None, KadiFilesizeMismatchError),
        (b"x", 1, "test", KadiChecksumMismatchError),
    ],
)
def test_save_upload_data_error(
    file_data, upload_size, checksum, exception, db, dummy_file, new_upload
):
    """Test if saving invalid upload data works correctly."""
    upload = new_upload(size=upload_size, checksum=checksum, file=dummy_file)

    # Start a new savepoint, so only the latest changes are rolled back.
    db.session.begin_nested()

    with pytest.raises(exception):
        save_upload_data(upload, BytesIO(file_data))

    assert upload.state == UploadState.INACTIVE
    assert dummy_file.state == FileState.ACTIVE
    assert dummy_file.revisions.count() == 1
    assert dummy_file.storage.exists(
        dummy_file.storage.create_filepath(str(dummy_file.id))
    )

# Copyright 2023 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import timedelta

import pytest
from flask import current_app

from kadi.lib.oauth.models import OAuth2ClientToken
from kadi.lib.oauth.models import OAuth2ServerAuthCode
from kadi.lib.oauth.models import OAuth2ServerClient
from kadi.lib.oauth.models import OAuth2ServerToken
from kadi.lib.utils import utcnow


def test_oauth2_client_token_is_expired(dummy_user):
    """Test if the expiration date of OAuth2 client tokens works correctly."""
    oauth2_client_token = OAuth2ClientToken.create(
        user=dummy_user, name="test", access_token="test"
    )
    assert not oauth2_client_token.is_expired

    oauth2_client_token.expires_at = utcnow() - timedelta(seconds=1)
    assert oauth2_client_token.is_expired


def test_oauth2_server_client_create(new_oauth2_server_client):
    """Test if creating new OAuth2 server clients works correctly."""
    client_secret = OAuth2ServerClient.new_client_secret()
    oauth2_server_client = new_oauth2_server_client(client_secret=client_secret)

    assert (
        OAuth2ServerClient.hash_client_secret(client_secret)
        == oauth2_server_client.client_secret
    )


def test_oauth2_server_token_update_client_metadata(db, dummy_oauth2_server_client):
    """Test if updating the client metadata of OAuth2 server clients works correctly."""
    prev_timestamp = dummy_oauth2_server_client.last_modified

    dummy_oauth2_server_client.update_client_metadata(
        client_name=dummy_oauth2_server_client.client_name
    )
    db.session.commit()

    assert dummy_oauth2_server_client.last_modified == prev_timestamp

    dummy_oauth2_server_client.update_client_metadata(
        client_name=f"{dummy_oauth2_server_client.client_name}-test"
    )
    db.session.commit()

    assert dummy_oauth2_server_client.last_modified != prev_timestamp


def test_oauth2_server_client_check_client_secret(new_oauth2_server_client):
    """Test if checking the client secret of OAuth2 server clients works correctly."""
    client_secret = OAuth2ServerClient.new_client_secret()
    oauth2_server_client = new_oauth2_server_client(client_secret=client_secret)

    assert oauth2_server_client.check_client_secret(client_secret)


def test_oauth2_server_token_is_expired(new_oauth2_server_token):
    """Test if the expiration date of OAuth2 server tokens works correctly."""
    oauth2_server_token = new_oauth2_server_token()
    assert not oauth2_server_token.is_expired

    oauth2_server_token.issued_at = (
        oauth2_server_token.issued_at - oauth2_server_token.expires_in - 1
    )
    assert oauth2_server_token.is_expired


@pytest.mark.parametrize("include_prefix", [True, False])
def test_oauth2_server_token_get_by_access_token(
    include_prefix, new_oauth2_server_token
):
    """Test if retrieving OAuth2 server tokens via access tokens works correctly."""
    access_token = OAuth2ServerToken.new_access_token(include_prefix=include_prefix)
    oauth2_server_token = new_oauth2_server_token(access_token=access_token)

    assert OAuth2ServerToken.get_by_access_token(access_token) == oauth2_server_token


def test_oauth2_server_token_get_by_refresh_token(new_oauth2_server_token):
    """Test if retrieving OAuth2 server tokens via refresh tokens works correctly."""
    refresh_token = OAuth2ServerToken.new_refresh_token()
    oauth2_server_token = new_oauth2_server_token(refresh_token=refresh_token)

    assert OAuth2ServerToken.get_by_refresh_token(refresh_token) == oauth2_server_token


@pytest.mark.parametrize("include_prefix", [True, False])
def test_oauth2_server_token_create(include_prefix, new_oauth2_server_token):
    """Test if creating new OAuth2 server tokens works correctly."""
    access_token = OAuth2ServerToken.new_access_token(include_prefix=include_prefix)
    refresh_token = OAuth2ServerToken.new_refresh_token()

    oauth2_server_token = new_oauth2_server_token(
        access_token=access_token, refresh_token=refresh_token
    )

    assert (
        OAuth2ServerToken.hash_token(access_token) == oauth2_server_token.access_token
    )
    assert (
        OAuth2ServerToken.hash_token(refresh_token) == oauth2_server_token.refresh_token
    )


def test_oauth2_server_auth_code_is_expired(db, dummy_oauth2_server_client, dummy_user):
    """Test if the expiration date of OAuth2 authorization codes works correctly."""
    oauth2_server_auth_code = OAuth2ServerAuthCode.create(
        user=dummy_user,
        client=dummy_oauth2_server_client,
        code="test",
        redirect_uri="test",
    )
    db.session.commit()

    assert not oauth2_server_auth_code.is_expired()

    expires_in = current_app.config["OAUTH_AUTH_CODE_EXPIRES_IN"]
    oauth2_server_auth_code.auth_time -= expires_in + 1

    assert oauth2_server_auth_code.is_expired()

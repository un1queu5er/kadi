#!/usr/bin/env bash
set -e

cd "$(dirname $(readlink -f $0))/.."

# Install Node.js.
apt update
apt install -y curl
curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
apt install -y nodejs

# Install the application with all development requirements.
pip3 install -e .[dev]

# Use the development environment when using the Kadi CLI.
export KADI_ENV="development"

# Compile the assets and translations.
kadi assets build
kadi i18n compile

# Build the wheel.
python3 -m build --wheel

# Upload the wheel to PyPI.
twine upload dist/*
